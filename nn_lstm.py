import numpy as np
import numpy.linalg as la
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import argparse
import progressbar
import data_handler as data
import os.path
from torch.optim.lr_scheduler import ReduceLROnPlateau

class LSTM(nn.Module):
	def __init__(self, input_dim, hidden_dim, n_layers, output_dim):
		super().__init__()

		self.n_layers = n_layers
		self.hidden_dim = hidden_dim

		self.lstm = nn.LSTM(input_dim, hidden_dim, n_layers,batch_first=True)
		self.fc = nn.Linear(hidden_dim,output_dim)

	def forward(self,x, hidden):
		x, hidden = self.lstm(x, hidden)

		prediction = self.fc(x)
		return prediction, hidden, x

	def init_hidden(self, batch_size):
		weight = next(self.parameters()).data
		hidden = (weight.new(self.n_layers, batch_size, self.hidden_dim).zero_(), weight.new(self.n_layers, batch_size, self.hidden_dim).zero_())
		return hidden

class Loss(nn.Module):
	def __init__(self):
		super().__init__()

	def forward(self, y, y_):
		loss = ((y-y_)**2).sum(axis=1)
		return loss.mean()

class Loss_L2(nn.Module):
	def __init__(self,M):
		super().__init__()
		self.M = torch.from_numpy(M).float()

	def set_batch_size(self,batch_size):
		self.batch_size = batch_size

	def forward(self, y, y_):
		e = (y-y_)

		temp = torch.matmul(self.M,e.permute(0,2,1))
		res = torch.matmul(e,temp)
		loss = torch.zeros(self.batch_size)
		for i in range(self.batch_size):
			loss[i] = torch.diag( res[i] ).sum()/100

		return loss.mean()


def train_model(model_path, hidden_dim, n_layers):
	path = 'torch_raw.npz'
	train_data = data.data_holder(batch_size=100)
	train_data.read_from_file(path)
#	train_data.to_torch()
	train_data.shuffle_noise()
	batch_size = train_data.batch_size
	num_batches = train_data.num_batches
	in_dim = train_data.data_dim_in
	out_dim = train_data.data_dim_out

	lstm_model = LSTM(in_dim, hidden_dim, n_layers, out_dim)

	if( os.path.isfile(model_path) == True ):
		lstm_model.load_state_dict(torch.load(model_path))

	device = torch.device('cpu')
	optimizer = optim.Adam(lstm_model.parameters(),lr=0.001)

	#mass_mat = np.load('Ar.npz')
	#loss_net = Loss_L2(mass_mat['Ar'])
	#loss_net.set_batch_size(batch_size)
	loss_net = Loss()

	max_iter = 5000

	for i in range(max_iter):
		lstm_model.train()
		#h = lstm_model.init_hidden(batch_size)

		train_data.shuffle_noise()
		for j in range(num_batches):
			optimizer.zero_grad()
			h = lstm_model.init_hidden(batch_size)
			h = tuple([e.data for e in h])
			output, h, dummy = lstm_model(train_data.torch_in[j], h)

			loss = loss_net(train_data.torch_out[j],output)
			loss.backward()

			optimizer.step()
			#print(loss)
		torch.save( lstm_model.state_dict(), model_path )

		lstm_model.eval()
		h = lstm_model.init_hidden(batch_size)
		h = tuple([e.data for e in h])
		output, h, dummy = lstm_model(train_data.torch_in[0], h)
		loss = loss_net(train_data.torch_out[0],output)
		print(loss)

class network_eval():
	def __init__(self, model_path):
		self.model = LSTM(110, 200, 2, 100)
		self.model.load_state_dict(torch.load(model_path))
		self.model.eval()
		self.batch_size = 1

		self.h = self.model.init_hidden(self.batch_size)

	def eval_model(self, in_vec):

		h_temp = tuple([e.data for e in self.h])
		output, self.h, dummy = self.model(in_vec, h_temp)

		return np.squeeze( np.asarray (output.detach().numpy() ) )

	def set_h_zero(self):
		self.h = self.model.init_hidden(self.batch_size)


if __name__== '__main__':
	train_model('model.pt', 200, 2)
