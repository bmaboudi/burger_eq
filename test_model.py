import numpy as np
import torch
import high_fidelity as hf
import nn_lstm as neural_net
import time

prob = hf.high_fidelity()
prob.variational_formulation()

params = np.random.uniform(1,2,10)
prob.set_rf_params(params)

t = time.time()
snaps = prob.time_stepping()
t1 = t - time.time()

#temp_data = np.load('data.npz')

#params = temp_data['in_data'][3]
#snaps = temp_data['out_data'][3]

data_dim = snaps.shape[1]
num_params = params.shape[0]
MAX_ITER = snaps.shape[0]

in_data = np.zeros( [1, data_dim + num_params] )
in_data[0,:num_params] = params
in_data[0,num_params:] = snaps[0]

in_data = np.reshape(in_data,[1,1,data_dim + num_params])

in_list = []
in_list.append( np.copy(in_data[0,0,num_params:]) )

nn = neural_net.network_eval('./model.pt')
nn_eval = nn.eval_model

for i in range(1):
	torch_in = torch.from_numpy( in_data ).float()

	out_vec = nn_eval(torch_in)
	in_list.append(np.copy(out_vec))

	in_data[ :, :, num_params: ] = snaps[i+1]

t = time.time()
for i in range(1,MAX_ITER-1):
	torch_in = torch.from_numpy( in_data ).float()

	out_vec = nn_eval(torch_in)
	in_list.append(np.copy(out_vec))

	in_data[ :, :, num_params: ] = out_vec
t2 = t - time.time()

print( t1/t2 )

in_list = np.reshape(in_list,[len(in_list),-1])
np.savez('res.npz',exact=snaps,approx=in_list)