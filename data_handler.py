import numpy as np
import scipy as scp
import torch

class data_holder():
	def __init__(self,batch_size=100):
		self.data_in = []
		self.data_out = []
		self.torch_in = []
		self.torch_out = []

		self.batch_size = batch_size

	def read_from_file(self, path):
		raw_data = np.load(path)
		self.data_in = raw_data['in_data']
		self.data_out = raw_data['out_data']

		self.data_dim_in = self.data_in.shape[2]
		self.data_dim_out = self.data_out.shape[2]

		num_data = self.data_in.shape[0]
		self.num_batches = int( num_data/self.batch_size )
		self.num_data = self.num_batches*self.batch_size

		self.shuffle_noise()

	def shuffle(self):
		self.torch_in.clear()
		self.torch_out.clear()

		seq_size = self.data_in.shape[1]
		vec_size_in = self.data_in.shape[2]
		vec_size_out = self.data_out.shape[1]

		rand_list = np.random.permutation( self.num_data )
		for i in range(self.num_batches):
			local_in = []
			local_out = []
			for j in range(self.batch_size):
				idx = rand_list[i*self.batch_size + j]
				local_in.append(self.data_in[idx])
				local_out.append(self.data_out[idx])
			local_in = np.reshape(local_in,[self.batch_size,seq_size,vec_size_in])
			local_out = np.reshape(local_out,[self.batch_size,vec_size_out])
			self.torch_in.append( torch.from_numpy( local_in ).float() )
			self.torch_out.append( torch.from_numpy( local_out ).float() )

	def shuffle_noise(self):
		self.torch_in.clear()
		self.torch_out.clear()

		seq_size = self.data_in.shape[1]
		vec_size_in = self.data_in.shape[2]
		vec_size_out = self.data_out.shape[2]

		num_params = vec_size_in - vec_size_out

		rand_list = np.random.permutation( self.num_data )
		for i in range(self.num_batches):
			local_in = []
			local_out = []
			for j in range(self.batch_size):
				idx = rand_list[i*self.batch_size + j]
				noise = np.zeros_like(self.data_in[idx])
				noise[:,num_params:] = np.random.normal(0,0.003,self.data_out[idx].shape)
				local_in.append(self.data_in[idx] + noise)
				local_out.append(self.data_out[idx])
			local_in = np.reshape(local_in,[self.batch_size,seq_size,vec_size_in])
			local_out = np.reshape(local_out,[self.batch_size,seq_size, vec_size_out])
			self.torch_in.append( torch.from_numpy( local_in ).float() )
			self.torch_out.append( torch.from_numpy( local_out ).float() )

	def to_torch(self):
		self.torch_in.clear()
		self.torch_out.clear()

		for i in range(self.num_batches):
			local_in = []
			local_out = []
			for j in range(self.batch_size):
				local_in.append(self.data_in[i*self.batch_size + j])
				local_out.append(self.data_out[i*self.batch_size + j])
			local_in = np.reshape(local_in,[self.batch_size,-1])
			local_out = np.reshape(local_out,[self.batch_size,-1])
			self.torch_in.append( torch.from_numpy( local_in ).float() )
			self.torch_out.append( torch.from_numpy( local_out ).float() )

if __name__ == '__main__':
	data = data_holder(batch_size=40)
	data.read_from_file('torch_raw.npz')
	data.shuffle_noise()

	print(data.torch_out[0].shape)
	print(data.torch_in[0].shape)
	#data.to_torch()
	#data.energy()

