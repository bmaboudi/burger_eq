from dolfin import *
import numpy as np
import progressbar

set_log_level(50)

class PeriodicBoundary(SubDomain):
	def inside(self, x, on_boundary):
		return bool(x[0] < DOLFIN_EPS and x[0] > -DOLFIN_EPS and on_boundary)
	def map(self, x, y):
		y[0] = x[0] - 1.0

class GField(UserExpression):
	def set_params(self, p):
		self.params = p
		self.num_params = self.params.shape[0]
	def eval(self, values, x):
		values[0] = 0.5
		for i in range(1,self.num_params+1):
			values[0] += 2.5*self.params[i-1]*np.sin(2*np.pi*i*x)/i**1.5/self.num_params

		#print(values[0])

class MyFunction(UserExpression):
	def eval(self, values, x):
		values[0] = np.sin(2*np.pi*x)

class high_fidelity():
	def __init__(self):
		self.mesh = UnitIntervalMesh(512)

		self.V = FunctionSpace(self.mesh, 'DG', 1, constrained_domain=PeriodicBoundary())
		FEM_el = self.V.ufl_element()
		ic = MyFunction(element = FEM_el)

		num_params = 10
		params = np.random.uniform(1,2,num_params)
		self.a = GField(element = FEM_el)

		self.u0 = Function(self.V)
		self.u_old = Function(self.V)
		self.u0 = interpolate(ic,self.V )

		self.v = TestFunction(self.V)
		self.u = TrialFunction(self.V)

		self.dt = 0.0005
		self.MAX_ITER = 400

#		self.h = CellSize(self.mesh)
		self.n = FacetNormal(self.mesh)

	def set_rf_params(self, p):
		self.a.set_params(p)

	def save_rand_field(self):
		f = Function(self.V)
		inter_f = interpolate(self.a,self.V)
		x = np.linspace(0,1,100)

		vals = []
		for i in range(x.shape[0]):
			vals.append( inter_f(x[i]) )

		vals = np.reshape(vals,[-1])
		np.savez('rand_field.npz',rf = vals)


	def variational_formulation(self):
		self.Flux = 0.25*(self.a('-')*self.u_old('-')**2 + self.a('+')*self.u_old('+')**2) - 0.5*(self.u_old('-') - self.u_old('+'))

		self.A = self.u*self.v*dx
		self.R = self.u_old*self.v*dx + self.dt*0.5*self.a*self.u_old**2*self.v.dx(0)*dx - self.dt*self.Flux*jump(self.v)*dS


	def time_stepping(self):
		sol = Function(self.V)

		snaps = []
		num_points = 100
		x = np.linspace(0,1,num_points)

		self.u_old.assign(self.u0)

		for i in range(self.MAX_ITER):
			solve(self.A==self.R, sol)
			self.u_old.assign(sol)

			local_snap = []
			for j in range(num_points):
				local_snap.append( sol(x[j]) )

			snaps.append( np.reshape( local_snap, [-1] ) )
		
		#np.savez('res.npz',snaps=np.reshape(snaps,[self.MAX_ITER,-1]))
		return np.reshape(snaps,[self.MAX_ITER,-1])

def generate_data(num_samps):
	hf = high_fidelity()
	hf.variational_formulation()

	in_data = []
	out_data = []
	#for i in range(num_samps):
	for i in progressbar.progressbar(range(num_samps)):
		params = np.random.uniform(1,2,10)
		hf.set_rf_params(params)
		hf.save_rand_field()
		local_in = hf.time_stepping()

		in_data.append(params)
		out_data.append(local_in)

	in_data = np.reshape(in_data,[num_samps,-1])
	out_data = np.reshape(out_data,[num_samps,out_data[0].shape[0],-1])

	np.savez('data.npz',in_data=in_data,out_data=out_data)

def data_organizer():
	data = np.load('data.npz')
	params = data['in_data']
	out = data['out_data']

	num_samps = params.shape[0]
	num_params = params.shape[1]
	seq_len = out.shape[1]
	data_dim = out.shape[2]

	in_data = []
	out_data = []
	for i in range(num_samps):
		local_in = np.zeros( [seq_len-1,data_dim+num_params] )
		p = np.reshape( params[i] , [1,-1])
		p = np.repeat(p,seq_len-1,axis=0)
		local_in[:,:num_params] = p
		local_in[:,num_params:] = out[i,:-1,:]
		#temp = np.repeat(pasrams[i], , axis=0)

		local_out = np.zeros( [seq_len-1,data_dim] )
		local_out = out[i,1:,:]

		in_data.append(local_in)
		out_data.append(local_out)

	in_data = np.reshape(in_data,[num_samps,seq_len-1,-1])
	out_data = np.reshape(out_data,[num_samps,seq_len-1,-1])

	np.savez('torch_raw.npz',in_data=in_data,out_data=out_data)

if __name__ == '__main__':
	#hf = high_fidelity()
	#hf.save_rand_field()
	#hf.variational_formulation()

	#num_samps = 100
	#generate_data(num_samps)

	data_organizer()