import matplotlib.pyplot as plt
from matplotlib import animation
import numpy as np 

fig = plt.figure()
ax1 = plt.axes(xlim=(0,1), ylim=(-1.1, 1.5))
line, = ax1.plot([], [], lw=2)

plotlays, plotcols = [2], ["black","red"]
lines = []
for index in range(2):
    lobj = ax1.plot([],[],lw=2,color=plotcols[index])[0]
    lines.append(lobj)


def init():
    for line in lines:
        line.set_data([],[])
    return lines

x1,y1 = [],[]
x2,y2 = [],[]

res = np.load('res.npz')
exact = res['exact']
approx = res['approx']
x = np.linspace(0,1,100)

gps_data = [ exact, approx ]


def animate(i):
    #for index in range(0,1):
    for lnum,line in enumerate(lines):
        line.set_data(x, gps_data[lnum][i,:]) # set data for each line separately. 

    return lines

# call the animator.  blit=True means only re-draw the parts that have changed.
anim = animation.FuncAnimation(fig, animate, init_func=init, frames=exact.shape[0], interval=20, blit=True)


plt.show()